require 'spec_helper'

describe Bowling do
  subject { Bowling::Game.new(inputs) }

  context 'game 1 score' do
    let(:inputs) { '10,10,10,10,10,10,10,10,10,10,10,10' }
    let(:score) { 300 }

    it { expect(subject.score).to eq score }
  end

  context 'game 2 score' do
    let(:inputs) { '1,2,3,4,5,5' }
    let(:score) { 20 }

    it { expect(subject.score).to eq score }
  end

  context 'game 3 score' do
    let(:inputs) { '9,1,10,8,0,2' }
    let(:score) { 48 }

    it { expect(subject.score).to eq score }
  end

  context 'game 4 score' do
    let(:inputs) { '10,0,0,9,1,0,0,8,2,0,0,7,3,0,0,6,4,0,0' }
    let(:score) { 50 }

    it { expect(subject.score).to eq score }
  end
end
