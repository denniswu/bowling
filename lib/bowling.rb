require 'bowling/version'

module Bowling
  class Game
    def initialize(inputs)
      @balls = []
      previous_ball = nil
      inputs.split(',').map(&:to_i).each do |pins|
        new_ball = Ball.new(pins, previous_ball)
        @balls << new_ball
        previous_ball = new_ball
      end
      @frames = []
      current_frame = Frame.new
      @frames << current_frame
      @balls.each do |ball|
        current_frame.balls << ball
        if (current_frame.strike? || current_frame.balls.length == 2) && @frames.length < 10
          current_frame = Frame.new
          @frames << current_frame
        end
      end
    end

    def score
      @frames.map(&:score).reduce(:+) || 0
    end
  end

  class Frame
    attr_accessor :balls

    def initialize
      @balls = []
    end

    def score
      temp_score = raw_score
      if (strike? || spare?) && @balls.last.next_ball
        temp_score += @balls.last.next_ball.pins
        if strike? && @balls.last.next_ball.next_ball
          temp_score += @balls.last.next_ball.next_ball.pins
        end
      end
      temp_score || 0
    end

    def raw_score
      @balls.map(&:pins).reduce(:+)
    end

    def strike?
      @balls.size == 1 && @balls.first.pins == 10
    end

    def spare?
      @balls.size == 2 && @balls[0..1].map(&:pins).reduce(:+) == 10
    end
  end

  class Ball
    attr_accessor :pins, :next_ball

    def initialize(pins, previous_ball)
      @pins = pins
      @next_ball = nil
      previous_ball.next_ball = self unless previous_ball.nil?
    end
  end
end
